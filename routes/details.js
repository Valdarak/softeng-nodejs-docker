const db = require('../db/db'),
  validator = require('../validator/validator');

exports.getDetails = (req, res) => {
  db.findAllClassesInformation()
    .then((classesDetInf) => {
      console.log('Showing details page for all!');
      res.status(200).render('details', { classesDetailedInformation: classesDetInf, errors: '', name: req.session.username });
    })
    .catch((err) => {
      res.status(404).render('error', { statusCode: 404, message: `Couldn't show details page!Error: ${err}`, name: req.session.username });
    });
};

exports.addClass = (req, res) => {
  const allClassesInformationP = db.findAllClassesInformation();
  const checkSubjectP = db.checkSubjectExists(req.body.subjectName);

  Promise.all([allClassesInformationP, checkSubjectP])
    .then(async ([classesDetInf, checkSubject]) => {
      let errMsg = validator.checkNonDBInf(req.body);

      if (checkSubject.length !== 0 && req.body.updateInf.toString() === 'no') {
        errMsg += 'Please select yes in the dropdown if you want to update existing subject information!<br>';
      }
      if (errMsg) {
        res.status(200).render('details', { classesDetailedInformation: classesDetInf, errors: errMsg, name: req.session.username });
        throw new Error('rendered');
      } else {
        try {
          if (checkSubject.length === 0) {
            await db.insertNewSubject(req.body);
          } else {
            await db.updateInfSubject(req.body);
          }
          return;
        } catch (err) {
          throw new Error(`Couldn't insert new subject: ${err.message}`);
        }
      }
    })
    .then(async () => {
      try {
        console.log('New class insert');
        await db.insertNewClass(req.body, req.session.username);
        return;
      } catch (err) {
        throw new Error(`Couldn't insert new class: ${err.message}`);
      }
    })
    .then(async () => {
      res.status(200).redirect('/requests/home');
    })
    .catch((err) => {
      if (err.toString() !== 'Error: rendered') {
        res.status(404).render('error', { statusCode: 404, message: `Couldn't check database!Error: ${err}`, name: req.session.username });
      }
    });
};

exports.getClasses = (req, res) => {
  db.findAllClassesInformation(req.params.subjectName)
    .then((classesDetInf) => {
      res.status(200).render('details', {
        classesDetailedInformation: classesDetInf, errors: '', uploadOK: 'OK', name: req.session.username,
      });
    })
    .catch((err) => {
      res.status(404).render('error', { statusCode: 404, message: `Couldn't get /details_:subjectName!Error: ${err}`, name: req.session.username });
    });
};
