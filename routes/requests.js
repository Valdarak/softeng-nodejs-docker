const express = require('express'),
  fs = require('fs'),
  path = require('path'),
  eformidable = require('express-formidable'),
  bodyparser = require('body-parser'),
  authMiddle = require('../middleware/authMiddle.js'),
  auth = require('./auth'),
  details = require('./details'),
  home = require('./home'),
  fileHandling = require('./fileHandling');


const router = express.Router();

// Loads main page with subject information (minimal information)
router.get(['/', '/home'], (req, res) => {
  home.getHome(req, res);
});

// Upon clicking on details on the navigation bar or clicking the 'i' icon on the home page it loads
// detailed information on the subject and all it's classes
router.get('/details', authMiddle.logged, (req, res) => {
  details.getDetails(req, res);
});

// Loads the register page
router.get('/register', authMiddle.notLogged, (req, res) => {
  auth.getRegister(req, res);
});

// Validates the register form and saves the information in the database
router.post('/register', authMiddle.notLogged, express.urlencoded({ extended: true }), (req, res) => {
  auth.register(req, res);
});

// If no error has been found in the form we insert the new subject (if not existing)
// and new class into the database
// If the subject already exists then we update the subject information.
router.post('/addClass', authMiddle.logged, authMiddle.teacher, express.urlencoded({ extended: true }), (req, res) => {
  details.addClass(req, res);
});

// Upon clicking 'i' on the gome page we load this page which brings up
// all classes belonging to the certain subject
router.get('/details_:subjectName', authMiddle.logged, express.urlencoded({ extended: true }), (req, res) => {
  details.getClasses(req, res);
});

// Upon clicking the delete button we delete the subject and all classes belonging to it
// Only teachers can delete classes
router.post('/delete', authMiddle.logged, bodyparser.json(), authMiddle.teacher, express.urlencoded({ extended: true }), async (req, res) => {
  home.deleteClass(req, res);
});

// Gets the login page
router.get('/login', authMiddle.notLogged, (req, res) => {
  auth.getLogin(req, res);
});

// Loggs the user in if his username and password match the ones stored
// in the database
router.post('/login', authMiddle.notLogged, express.urlencoded({ extended: true }), (req, res) => {
  auth.login(req, res);
});

const uploadDir = path.join(__dirname, '../syllabus');

if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

router.use(eformidable({ uploadDir }));

// We upload the syllabus into the syllabus/subjectName folder and save the filename to
// the database
router.post('/uploadSyl_:subjectName', authMiddle.logged, authMiddle.teacher, (req, res) => {
  fileHandling.uploadSyl(req, res);
});

// Gets the name of the file associated with the subject and downloads the syllabus belonging to it
router.get('/download_:subjectName', authMiddle.logged, (req, res) => {
  fileHandling.downloadSyl(req, res);
});

// Logs out the user
router.get('/logout', authMiddle.logged, (req, res) => {
  auth.logout(req, res);
});

router.get('/subjectDetails_:subjectName', (req, res) => {
  home.getSubjectDetails(req.params.subjectName, req, res);
});

module.exports = router;
