const db = require("../db/db");

exports.getHome = (req, res) => {
  db.findAllSubjects()
    .then((subjectInf) => {
      console.log("Showing home page!");
      res
        .status(200)
        .render("home", { subjectInf, errors: "", name: req.session.username });
    })
    .catch((err) => {
      res
        .status(404)
        .render("error", {
          statusCode: 404,
          message: `Couldn't show home page!Error: ${err}`,
          name: req.session.username,
        });
    });
};

exports.deleteClass = async (req, res) => {
  await db
    .deleteClass(req)
    .then(async () => {
      res.status(200).send("Delete is successful!");
    })
    .catch((err) => {
      console.log(err);
      res
        .status(500)
        .render("error", {
          statusCode: 500,
          message: `${err}`,
          name: req.session.username,
        });
    });
};

exports.getSubjectDetails = (subjectName, req, res) => {
  db.getSubjectDetails(subjectName)
    .then((result) => {
      res.status(200).send(JSON.stringify(result));
    })
    .catch((err) => {
      res
        .status(500)
        .render("error", {
          statusCode: 500,
          message: `${err}`,
          name: req.session.username,
        });
    });
};
