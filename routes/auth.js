// Contains the methods required for executing the authentication methods: register, login, logout

const crypto = require("crypto"),
  validator = require("../validator/validator"),
  db = require("../db/db");

const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = "sha512",
  iterations = 2000;

exports.getLogin = (req, res) => {
  res.status(200).render("login", { name: req.session.username });
};

exports.getRegister = (req, res) => {
  try {
    res
      .status(200)
      .render("register", { errors: "", name: req.session.username });
  } catch (err) {
    res
      .status(404)
      .render("error", {
        statusCode: 404,
        message: `Couldn't show register page! ${err}`,
        name: req.session.username,
      });
  }
};

exports.register = (req, res) => {
  const { userName } = req.body;
  const { password } = req.body;
  const { confirmPassword } = req.body;

  let errMsg = validator.checkUsername(userName);
  errMsg += validator.checkPassword(password);

  if (password !== confirmPassword) {
    errMsg += "Passwords are not equal. Please rewrite them!";
  }

  if (errMsg) {
    res.status(404).render("register", { errors: errMsg });
  } else {
    //const salt = crypto.randomBytes(saltSize);
    //const hash = crypto.pbkdf2Sync(password, salt, iterations, hashSize, hashAlgorithm);

    //const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');

    // Only student can be inserted from database. Admin can grant teacher roles only, from back-end
    db.insertUser(userName, password, " ".toString(), "student")
      .then(() => {
        res.status(200).render("register", {
          errors: "Succesfully inserted new user!<br>",
        });
      })
      .catch((error) => {
        if (error.toString().search("Duplicate entry") < 0) {
          res
            .status(404)
            .render("error", {
              statusCode: 404,
              message: `Couldn't insert new user into database : ${error.message}`,
              name: req.session.username,
            });
        } else {
          res
            .status(200)
            .render("register", {
              errors: `There is already an user with the name ${userName}!<br>`,
              name: req.session.username,
            });
        }
      });
  }
};

exports.login = (req, res) => {
  db.getUser(req.body.userName)
    .then((result) => {
      if (result.length > 0) {
        //const expectedHash = result[0].password.substring(0, hashSize * 2);
        //const salt = Buffer.from(result[0].password.substring(hashSize * 2), 'hex');

        const pass = req.body.password;

        //const binaryHash = crypto.pbkdf2Sync(pass, salt, iterations, hashSize, hashAlgorithm);

        //const actualHash = binaryHash.toString('hex');

        if (pass === result[0].password) {
          /*(actualHash === expectedHash)*/ db.getRole(req.body.userName)
            .then((userRole) => {
              if (userRole) {
                req.session.role = userRole[0].role;
                req.session.username = req.body.userName;
                res.status(200).redirect("/requests/home");
              } else {
                res
                  .status(500)
                  .render("error", {
                    statusCode: 500,
                    message: "Username has not role in database!",
                    name: req.session.username,
                  });
              }
            })
            .catch((e) => {
              res
                .status(500)
                .render("error", {
                  statusCode: 500,
                  message: `Couldn't get usernames's role!: ${e}`,
                  name: req.session.username,
                });
            });
        } else {
          res
            .status(200)
            .render("login", {
              errors: "Incorrect password!\n",
              name: req.session.username,
            });
        }
      } else {
        res
          .status(200)
          .render("login", {
            errors: "This user doesn't exist in the database!\n",
            name: req.session.username,
          });
      }
    })
    .catch((err) => {
      res
        .status(400)
        .render("error", {
          statusCode: 400,
          message: `Couldn't login!: ${err}`,
          name: req.session.username,
        });
    });
};

exports.logout = (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      res
        .status(404)
        .render("error", {
          statusCode: 404,
          message: `Couldn't logout! ${err}`,
          name: req.session.username,
        });
    } else {
      res.redirect("/requests/home");
    }
  });
};
