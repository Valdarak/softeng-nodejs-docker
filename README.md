# Webprogramozás laborfeladatok

1. Előszőr a setup.sql filet, kell futtatni, hogy jöjjön létre a kapcsolat az adatbázissal. (ott vannak további utasítások)

2. Ezután létre kell hozni a siteon három darab felhasználót. Az első kettőt, be kell állítani az adatbázisban teacher szerepkörre

3. Ezt közvetően le lehet futtatni az dbinsert.sql filet, hogy töltse fel az adatbázist tantárgyakkal, illetve órákkal

4. Pár tudnivaló könnyebb tesztelésre: 
    a) Home mindenkinek elérhető
    b) details oldal csak bejelentkezett felhasználóknak elérhető (bármilyen uton is ért el ide, pl.: ha az i-re kattintva ér ide el a felhasználó)
    c) törölni a főoldalról, csak teacher szerepkörű felhasználóknak szabad (akik be vannak jelentkezve természetesen)
    d) hozzádni egy class-t, vagy syllabust feltölteni, ugyancsak tanároknak szabad csak
    e) Meg lehet próbálni loginolni amiután már loginolt egyszer a felhasználó (nem látszik a gomb, de manuálisan beírva az url-t lehetséges), de nem fogja engedni
    f) az e)-hez hasonlóan a register, logout se müködnek ha be van már jelentkezve, vagy, ha nincs bejelentkezve
