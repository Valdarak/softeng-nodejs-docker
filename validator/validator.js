// Checks if username given in form is valid
// valid means at least length of 5 and username must contain at least on character
exports.checkUsername = (username) => {
  let errMsg = '';
  let errNum = 0;
  if (username.length < 5) {
    errNum += 1;
    errMsg += ` ${errNum}: Username length must be at least 5 characters!<br>`;
  }
  if (username.search(/^.*[a-zA-Z].*$/) < 0) {
    errNum += 1;
    errMsg += `${errNum}: Username must have at least one character!<br>`;
  }
  return errMsg;
};

// Password validity: must contain one digit, uppercase, lowercase letter and must have a
// length of at least 8 characters
exports.checkPassword = (password) => {
  let errMsg = '';
  let errNum = 0;

  const lowercase = /^.*[a-z].*$/;
  const uppercase = /^.*[A-Z].*$/;
  const digit = /^.*[0-9].*$/;

  if (password.length < 8) {
    errNum += 1;
    errMsg += `${errNum}: Password must be at least 8 chars long!<br>`;
  }

  const low = password.search(lowercase);
  const upp = password.search(uppercase);
  const dig = password.search(digit);

  if (low < 0 || upp < 0 || dig < 0) {
    errNum += 1;
    errMsg += `${errNum}: Password must contain at leat a lowercase, an uppercase letter and a digit!<br>`;
  }
  return errMsg;
};

// Checks if subject name contain one uppercase letter at the start and only lowercase letter
// after that
function validSubjectName(subjectName) {
  if (subjectName.match(/^[A-Z][a-z]*$/) === null) {
    return 'Subject name has to start with a capital letter and can only contain letters!<br>';
  }
  return '';
}

// Valid year value is between 1 and 10
function validYear(year) {
  if ((year.match(/^[1-9]$/) === null && year.match(/^10$/) === null) || parseInt(year, 10) < 1 || parseInt(year, 10) > 10) {
    return 'Year needs to be between 1 and 10!<br>';
  }
  return '';
}

// Checks if lecture, seminar, lab are numbers and the if they are postivie value.
// If they passed those checks program checks if total number is less than or equal to 120
function validHours(lecture, seminar, lab) {
  if (lecture.match(/^[0-9]*$/) === null || seminar.match(/^[0-9]*$/) === null || lab.match(/^[0-9]*$/) === null) {
    return 'Lecture, seminar, lab need to be numbers!<br>';
  }
  if (parseInt(lecture, 10) < 0 || parseInt(seminar, 10) < 0 || parseInt(lab, 10) < 0) {
    return 'Lecture, seminar, lab need to be positive number!<br>';
  }
  if (parseInt(lecture, 10) + parseInt(seminar, 10) + parseInt(lab, 10) >= 120) {
    return 'Lecture, seminar, lab total hours cannot be more than 120!<br>';
  }
  return '';
}

// Classroom can be all digits or all letters but now combination of those
function validClassroom(classRoom) {
  if (classRoom.match(/^[A-Z][a-z]*$/) === null && classRoom.match(/^[0-9]*$/) === null) {
    return 'Classroom needs to be all letters or all digits!<br>';
  }
  return '';
}

// Checks if time is between 08:00 and 20:00
function validTime(time) {
  if (time.match(/^0[89]:[0-5][0-9]$/) === null && time.match(/^1[0-9]:[0-5][0-9]$/) === null && time.match(/^20:00$/) === null) {
    return 'Time needs to be between 08:00 and 20:00!<br>';
  }
  return '';
}

// This function calls the validator functions and returns their message regarding
// the validity of the input values
exports.checkNonDBInf = (body) => {
  let errMsg = '';
  errMsg += validSubjectName(body.subjectName);
  errMsg += validYear(body.year);
  errMsg += validHours(body.lecture, body.seminar, body.lab);
  errMsg += validClassroom(body.classRoom);
  errMsg += validTime(body.time);

  return errMsg;
};

exports.checkClassInsert = (body) => {
  let errMsg = '';
  errMsg += validSubjectName(body.subjectName);
  errMsg += validClassroom(body.classRoom);
  errMsg += validTime(body.time);

  return errMsg;
};
