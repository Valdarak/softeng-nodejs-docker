// Modules

const mysql = require("mysql2");

// Creating a connection pool
const pool = mysql
  .createPool({
    connectionLimit: 10,
    database: "db",
    host: "db",
    port: 3306,
    user: "user",
    password: "user",
  })
  .promise();

async function runQuery(query, options = []) {
  return (await pool.query(query, options))[0];
}

function createSubjectsTable() {
  try {
    const query = `CREATE TABLE IF NOT EXISTS Subjects (
      subjectCode INT AUTO_INCREMENT PRIMARY KEY,
      subjectName varchar(50) NOT NULL UNIQUE,
      subjectYear INT NOT NULL, 
      lectureHours INT NOT NULL,
      seminarHours INT NOT NULL,
      labHours INT NOT NULL
      ); `;
    //runQuery(query);
    console.log("Succesfully created Subjects table!");
  } catch (err) {
    console.log(`Error creating Subjects table! Error: ${err}`);
  }
}

function createUsersTable() {
  try {
    const query = `CREATE TABLE IF NOT EXISTS Users (
      userID INT AUTO_INCREMENT PRIMARY KEY,
      user varchar(50) NOT NULL UNIQUE,
      password varchar(500) NOT NULL,
      salt varchar(150) NOT NULL,
      role varchar(30) NOT NULL
      ); `;
    //runQuery(query);
    console.log("Succesfully created Users table!");
  } catch (err) {
    console.log(`Error creating Users table! Error: ${err}`);
  }
}

function createClassesTable() {
  try {
    const query = `CREATE TABLE IF NOT EXISTS Classes (
      classCode INT PRIMARY KEY AUTO_INCREMENT,
      subjectCode INT,
      classDay VARCHAR(20) NOT NULL,
      classHour VARCHAR(5) NOT NULL,
      classRoom VARCHAR(50) NOT NULL,
      userID INT,
      fileName VARCHAR(100),
      FOREIGN KEY (subjectCode) REFERENCES Subjects(subjectCode),
      FOREIGN KEY (userID) REFERENCES Users(userID) 
      ); `;
    //runQuery(query);
    console.log("Succesfully created Classes table!");
  } catch (err) {
    console.log(`Error creating Classes table! Error: ${err}`);
  }
}

// Create the aforementioned tables
createSubjectsTable();
createUsersTable();
createClassesTable();

// Gets all information from the Subjects table
exports.findAllSubjects = async () => {
  //const query = 'SELECT subjectName, subjectYear, lectureHours, seminarHours, labHours FROM Subjects';
  return " "; //runQuery(query);
};

// Find all information regarding subjects, classes
exports.findAllClassesInformation = async (subjectName = "", id = -1) => {
  let query = `SELECT subjectName, subjectYear, lectureHours, seminarHours, labHours, classDay, classHour, classRoom, user
  FROM Classes
  INNER JOIN Users
  ON Users.userID = Classes.userID
  INNER JOIN Subjects
  ON Subjects.subjectCode = Classes.subjectCode`;
  if (subjectName) {
    query += ` WHERE subjectName = "${subjectName}"`;
  }
  if (id > 0) {
    query += ` WHERE classCode = "${id}"`;
  }
  return runQuery(query);
};

exports.findClassesBeforeHour = async (hour) => {
  const query = `SELECT subjectName, subjectYear, lectureHours, seminarHours, labHours, classDay, classHour, classRoom, user
  FROM Classes
  INNER JOIN Users
  ON Users.userID = Classes.userID
  INNER JOIN Subjects
  ON Subjects.subjectCode = Classes.subjectCode
  WHERE STRCMP(classHour, "${hour}") < 0`;

  return runQuery(query);
};

// Upon registering inserts a new user into the database
exports.insertUser = async (userName, hashWithSalt, salt, role) => {
  const query = `INSERT INTO Users(user,password, salt, role) VALUES("${userName}", "${hashWithSalt}", "${salt}", "${role}")`;
  return runQuery(query);
};

// Checks if Subject with give subject name exists
exports.checkSubjectExists = async (subjectName) => {
  const query = `SELECT * FROM Subjects WHERE subjectName = "${subjectName}"`;
  return runQuery(query);
};

// Inserts new subject into database
exports.insertNewSubject = async (body) => {
  const query = `INSERT INTO Subjects(subjectName, subjectYear, lectureHours, seminarHours, labHours)
                  VALUES ("${body.subjectName}", "${body.year}", "${body.lecture}", "${body.seminar}", "${body.lab}")`;
  return runQuery(query);
};

// Update info if we submit a form for an already existing database
exports.updateInfSubject = async (body) => {
  const query = `UPDATE Subjects
                  SET subjectYear = "${body.year}", lectureHours = "${body.lecture}", seminarHours = "${body.seminar}", labHours = "${body.lab}"
                  WHERE subjectName = "${body.subjectName}"`;
  return runQuery(query);
};

// Inserts new class
exports.insertNewClass = async (body, username) => {
  const query = `INSERT INTO Classes(subjectCode, classDay, classHour, classRoom, userID)
                  VALUES ((SELECT subjectCode FROM Subjects WHERE subjectName = "${body.subjectName}"), "${body.daylist}","${body.time}", "${body.classRoom}", (SELECT userID FROM Users WHERE user = "${username}"));`;
  return runQuery(query);
};

// deletes subject with given name
exports.deleteClass = async (req) => {
  const query = `DELETE FROM Classes
                  WHERE classDay = "${req.body.day}" AND classHour = "${req.body.hour}" AND classRoom = "${req.body.room}" AND (subjectCode = (SELECT subjectCode FROM Subjects WHERE subjectName = "${req.body.name}"))`;
  console.log(query);
  return runQuery(query);
};

exports.deleteClassByID = async (classCode) => {
  const query = `DELETE FROM CLASSES
                  WHERE classCode = "${classCode}"`;
  return runQuery(query);
};

exports.updateClassroom = async (classCode, classroom) => {
  const query = `UPDATE Classes
                  SET classRoom = "${classroom}"
                  WHERE classCode = "${classCode}"`;

  return runQuery(query);
};

// insert sname a syllabus uploaded
exports.insertFileName = async (fileName, subjectName) => {
  const query = `UPDATE Classes
                  SET fileName = "${fileName}"
                  WHERE subjectCode = (SELECT subjectCode FROM Subjects
                    WHERE subjectName = "${subjectName}")`;
  return runQuery(query);
};

// gets name of uploaded file
exports.getFileName = async (subjectName) => {
  const query = `SELECT fileName FROM Classes
                  WHERE subjectCode = (SELECT subjectCode FROM Subjects
                                        WHERE subjectName = "${subjectName}")`;
  return runQuery(query);
};

exports.getUser = async (userName) => {
  const query = `SELECT user, password, salt FROM Users WHERE user="${userName}"`;
  return runQuery(query);
};

exports.getRole = async (userName) => {
  const query = `SELECT role FROM Users WHERE user="${userName}"`;
  return runQuery(query);
};

exports.getSubjectDetails = async (subjectName) => {
  const query = `SELECT subjectYear, lectureHours, seminarHours, labHours FROM Subjects WHERE subjectName = "${subjectName}"`;
  return runQuery(query);
};
