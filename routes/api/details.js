const express = require('express'),
  bodyparser = require('body-parser'),
  authMiddle = require('../../middleware/authMiddle.js'),
  home = require('../home');

const router = express.Router();

// Upon clicking the delete button we delete the subject and all classes belonging to it
// Only teachers can delete classes
router.post('/delete', authMiddle.logged, bodyparser.json(), authMiddle.teacher, express.urlencoded({ extended: true }), async (req, res) => {
  home.deleteClass(req, res);
});

module.exports = router;
