// Checks if user is logged in
exports.logged = (req, res, next) => {
  if (!req.session.username) {
    res.status(403).render('accessdenied', { message: 'You need to login to view this page!', name: req.session.username  });
  } else next();
};

// Checks if user is not logged in
exports.notLogged = (req, res, next) => {
  if (req.session.username) {
    res.status(403).render('accessdenied', { message: 'You need to be logged out to perform this task!', name: req.session.username  });
  } else next();
};

// Checks if user checked in has role of teacher
exports.teacher = (req, res, next) => {
  if (req.session.role === 'teacher') {
    next();
  } else {
    res.status(403).render('accessdenied', { message: 'You need to be a teacher to perform this task!', name: req.session.username  });
  }
};

exports.validID = (req, res, next) => {
  if (+req.params.classID > 0) {
    next();
  } else {
    res.status(403).json({ message: `${req.params.classID} is not a valid id.` });
  }
};

exports.validHour = (req, res, next) => {
  if (req.params.classHour.match(/^0[89]:[0-5][0-9]$/) === null && req.params.classHour.match(/^1[0-9]:[0-5][0-9]$/) === null && req.params.classHour.match(/^20:00$/) === null) {
    res.status(403).json({ message: `${req.params.classHour} needs to be a time between 08:00 and 20:00` });
  } else next();
};
