const express = require("express"),
  path = require("path"),
  helpers = require("handlebars-helpers"),
  session = require("express-session"),
  ehandlebars = require("express-handlebars"),
  apiRoutes = require("./routes/api"),
  errorMiddleware = require("./middleware/error"),
  requestRoutes = require("./routes/requests");

const app = express();

app.use(
  session({
    secret: "142e6ecf42884f03",
    username: "",
    role: "",
    resave: false,
    saveUninitialized: true,
  })
);

// Setting up the static folder
app.use(express.static(path.join(__dirname, "static")));

// Set up handlesbars as template engine
app.set("view engine", "hbs");
app.set("views", path.join(__dirname, "views"));
app.engine(
  "hbs",
  ehandlebars({
    extname: "hbs",
    defaultView: "main",
    layoutsDir: path.join(__dirname, "views/layouts"),
    partialsDir: path.join(__dirname, "views/partials"),
    helper: helpers(),
  })
);

app.use("/requests", requestRoutes);

app.use("/api", apiRoutes);

app.use(errorMiddleware);

app.listen(process.env.PORT || 3000, () => {
  console.log("Server listening on http://localhost:3000/requests/ ...");
});
