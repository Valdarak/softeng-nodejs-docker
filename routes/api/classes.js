const express = require('express'),
  db = require('../../db/db'),
  authMiddle = require('../../middleware/authMiddle'),
  validator = require('../../validator/validator');

const router = express.Router();

// Find All
router.get('/', /* authMiddle.logged */ (req, res) => {
  db.findAllClassesInformation()
    .then((classesInf) => res.json(classesInf))
    .catch((err) => res.status(500).json({ message: `Error while finding all classes information! ${err.message}` }));
});

// FindByID
router.get('/:classID', /* authMiddle.logged, authMiddle.validID */ (req, res) => {
  db.findAllClassesInformation('', req.params.classID)
    .then((classesInf) => {
      if (classesInf.length !== 0) res.json(classesInf);
      else res.status(404).json({ message: `Couldn't find class with id: ${req.params.classID}` });
    })
    .catch((err) => res.status(500).json({ message: `Error while finding all classes information with specific id! ${err.message}` }));
});

// Find Classes with classes starting before parameter
router.get('/hour/before', authMiddle.logged, (req, res) => {
  db.findClassesBeforeHour(req.query.classHour)
    .then((classesInf) => {
      if (classesInf.length !== 0) res.json(classesInf);
      else res.status(404).json({ message: `Couldn't find class with hour before: ${req.params.classHour}` });
    })
    .catch((err) => res.status(500).json({ message: `Error while finding all classes information with before specific hour! ${err.message}` }));
});


// Insert new class if subject exists with that name
router.post('/', /* authMiddle.logged, authMiddle.teacher, */express.urlencoded({ extended: true }), (req, res) => {
  db.checkSubjectExists(req.body.subjectName)
    .then(async (checkSubject) => {
      let errMsg = validator.checkClassInsert(req.body);

      if (checkSubject.length === 0) {
        errMsg += 'Please select yes in the dropdown if you want to update existing subject information!<br>';
      }
      if (errMsg) {
        res.status(200).json({ message: `The following errors were found: ${errMsg}` });
      } else {
        db.insertNewClass(req.body, 'Valdarak');
        res.status(200).json({ message: 'Successfully inserted new class into database!' });
      }
    })
    .catch((err) => {
      res.status(500).json({ message: `${err.message}. Couldn't insert new class!` });
    });
});

router.delete('/:classCode', (req, res) => {
  db.deleteClassByID(req.params.classCode)
    .then((rows) => {
      if (rows.affectedRows === 0) res.status(404).json({ message: `No class with code ${req.params.classCode}.` });
      else res.status(200).json({ message: 'Successfully deleted class from database!' });
    })
    .catch((err) => {
      res.status(500).json({ message: `${err}. Couldn't delete class from database!` });
    });
});

/* change classroom of class specified by id */
router.patch('/:classCode', express.urlencoded({ extended: true }), (req, res) => {
  db.updateClassroom(req.params.classCode, req.body.newClassroom)
    .then((rows) => {
      if (rows.affectedRows === 0) res.status(404).json({ message: `No class with code ${req.params.classCode}.` });
      else res.status(200).json({ message: 'Successfully updated class in database!' });
    })
    .catch((err) => {
      res.status(500).json({ message: `${err}. Couldn't update class in database!` });
    });
});

module.exports = router;
