// Deals with pages not found
module.exports = (req, res) => {
  res.status(404).render('error', { statusCode: 404, message: 'The requested endpoint is not found', name: req.session.username });
};
