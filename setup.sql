-- Segédállomány, hogy előkészítsünk egy MySQL adatbázist a példaprogramnak.
-- Futtatás konzolról (UNIX rendszeren): 
--     mysql -u root -p <setup.sql

-- az alábbi sor törli az adatbázist ha nem létezik
-- DROP DATABASE IF EXISTS webprog;

-- készít egy adatbázist
CREATE DATABASE IF NOT EXISTS timetable;

-- készít egy felhasználót, aki minden műveletet végrehajthat ezen adatbázisban
USE timetable;

CREATE USER 'timetable'@'localhost' IDENTIFIED BY 'vaam0249';
GRANT ALL PRIVILEGES ON *.* TO 'timetable'@'localhost' WITH GRANT OPTION;

-- Mysql Workbench 8.0-tol ez is kell
-- ALTER USER 'timetable'@'localhost' IDENTIFIED WITH mysql_native_password BY 'vaam0249';
-- flush privileges;