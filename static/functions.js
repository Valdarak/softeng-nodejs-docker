/* eslint-disable-next-line */
function showDetails(subjectName) {
  fetch(`/api/home/subjectDetails_${subjectName}`)
    .then((res) => res.json())
    .then((resJSON) => {
      document.getElementById(`year${subjectName}`).innerHTML = resJSON[0].subjectYear;
      document.getElementById(`lecture${subjectName}`).innerHTML = resJSON[0].lectureHours;
      document.getElementById(`seminar${subjectName}`).innerHTML = resJSON[0].seminarHours;
      document.getElementById(`lab${subjectName}`).innerHTML = resJSON[0].labHours;

      document.getElementById(`year${subjectName}`).style.opacity = '100%';
      document.getElementById(`lecture${subjectName}`).style.opacity = '100%';
      document.getElementById(`seminar${subjectName}`).style.opacity = '100%';
      document.getElementById(`lab${subjectName}`).style.opacity = '100%';
    });
}

/* eslint-disable-next-line */
function deleteClass(subjectName, classDay, classHour, classRoom){
  fetch('/api/details/delete', {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      name: subjectName,
      day: classDay,
      hour: classHour,
      room: classRoom,
    }),
  })
    .then(() => {
      const row = document.getElementById(`${classDay}${classHour}${classRoom}`);
      row.parentNode.removeChild(row);
    })
    .catch((err) => {
      console.log(err);
    });
}
