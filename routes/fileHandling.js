const fs = require('fs'),
  db = require('../db/db');

exports.uploadSyl = (req, res) => {
  const fileHandler = req.files.upFile;

  if (fs.existsSync(`syllabus/${req.params.subjectName}/${req.params.subjectName}`)) {
    fs.unlinkSync(`syllabus/${req.params.subjectName}/${req.params.subjectName}`);
  }

  if (!fs.existsSync(`syllabus/${req.params.subjectName}`)) {
    fs.mkdirSync(`syllabus/${req.params.subjectName}`);
  }
  fs.renameSync(fileHandler.path, `syllabus/${req.params.subjectName}/${fileHandler.name}`);


  db.insertFileName(fileHandler.name, req.params.subjectName)
    .then(async () => {
      res.redirect(`/requests/details_${req.params.subjectName}`);
    })
    .catch((err) => {
      res.status(404).render('error', { statusCode: 404, message: `Couldn't insert fileName into database!Error: ${err}`, name: req.session.username });
    });
};

exports.downloadSyl = (req, res) => {
  db.getFileName(req.params.subjectName)
    .then(async (result) => {
      res.download(`syllabus/${req.params.subjectName}/${result[0].fileName}`, (err) => {
        if (err) {
          res.redirect(`/requests/details_${req.params.subjectName}`);
        }
      });
    })
    .catch((err) => {
      res.status(404).render('error', { statusCode: 404, message: `Couldn't download fileName into database!Error: ${err}`, name: req.session.username });
    });
};
