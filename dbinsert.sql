SELECT * FROM users;
SELECT * FROM classes;
SELECT * FROM subjects;

-- Syllabust a siteon kell feltölteni és csak akkor lesznek létrehozva a mappál
-- és az adatbázis fileName adattagjai

-- Létre kell hozni három felhasználót kliens oldalon, és kettőnek megkell változtani az AB-ben a roleját a követkző vel:
-- Első kettő kéne legyen, aki lesz a tanár 
UPDATE Users
SET role = 'teacher'
WHERE userID <= 2;

-- Ezután le lehet futtatni, a két insertet ez alatt, és lesz pár tantárgy, és óra
-- Syllabust fel kell tölteni, hogy lehessen letölthető

INSERT INTO Subjects(subjectName, subjectYear, lectureHours, seminarHours, labHours)
VALUES ('Algebra', 1, 12, 16, 19), ('Linux', 2, 40, 40, 25), ('Web', 3, 12, 12, 45),
('Differencialok', 7, 25, 35, 40);

INSERT INTO Classes(subjectCode, classDay, classHour, classRoom, userID)
VALUES (1, 'monday', '08:00', 'Gamma', 1), (1, 'wednesday', '12:00', 'Aula', 1),
(1, 'saturday', '17:00', '321', 2), (2, 'tuesday', '09:45', '001', 1), (2, 'sunday', '20:00', '123', 2),
(2, 'thursday', '15:20', 'Phi', 2), (3, 'monday', '12:30', 'E', 1), (1, 'friday', '11:20', 'Pi', 1); 