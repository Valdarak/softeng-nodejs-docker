const express = require('express'),
  home = require('../home');

const router = express.Router();

router.get('/subjectDetails_:subjectName', (req, res) => {
  home.getSubjectDetails(req.params.subjectName, req, res);
});

module.exports = router;
