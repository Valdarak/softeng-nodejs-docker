// Ez az állomány egy REST API gyökerének express routerét konfigurálja.
// Betölti a további API endpoint lekezelőket.
const express = require('express'),
  bodyParser = require('body-parser'),
  homeRoutes = require('./home'),
  detailsRoutes = require('./details'),
  classesRoutes = require('./classes');

const router = express.Router();

// minden testtel ellátott API hívás
// JSON-t tartalmaz
router.use(bodyParser.json());

// API endpointok a megfelelő alrouterbe
router.use('/home', homeRoutes);
router.use('/details', detailsRoutes);
router.use('/classes', classesRoutes);

module.exports = router;
